package controller2;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.Personalmedical;
import util.UtilHospital;

public class Logging {
	@FXML private Text userNameText;
	@FXML private Text paswordText;
	@FXML private Text errorText;
	@FXML private TextField textFieldUserName;
	@FXML private TextField textFieldPassword;
	@FXML private Button loggingButton;
	
	public void populateLoggingMenu() {
		UtilHospital utilDb = new UtilHospital();
		utilDb.setUp();
		utilDb.startTransaction();
		String userName = textFieldUserName.getAccessibleText();
		String password = textFieldPassword.getAccessibleText();
		boolean connected = false;
		while(connected == false){
			List<Personalmedical> doctorList = (List<Personalmedical>)utilDb.doctorList();
			for(Personalmedical doctor: doctorList) {
				if(doctor.getName() == userName && doctor.getPassword() == password)
					connected = true;
			}
			if(connected == false) {
				textFieldUserName.clear();
				textFieldPassword.clear();
				errorText.setText("Username or password wrong!!");
			}
			else{
				errorText.setText("Connected!!");
				}
		}
	}	
	/*public void onLoggingPressedEnterInMain() throws IOException {
		MainController.stage = new Stage();
		BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controller/FXML.fxml"));
		Scene scene = new Scene(root,700,700);
		MainController.stage.setScene(scene);
		MainController.stage.setTitle("Hospital");
		MainController.stage.centerOnScreen();
		MainController.stage.show();
		stage.close();
	}*/
	public void initialize() {
		populateLoggingMenu();
	}
}
