package main;
import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{
	@Override
	public void start(Stage primaryStage)throws Exception{
		/*try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controller/FXML.fxml"));
			primaryStage.setScene(scene);
			primaryStage.setTitle("Hospital");
			primaryStage.centerOnScreen();
			primaryStage.show();
			System.out.println("here2");
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		/*try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controller2/FxmlController.fxml"));
			Scene scene = new Scene(root,450,250);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Logging");
			primaryStage.centerOnScreen();
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("/controller2/FxmlController.fxml"));
			//BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controller/FXML.fxml"));
			@SuppressWarnings("static-access")
			BorderPane root = (BorderPane) fxmlLoader.load(getClass().getResource("/controller2/ControllerFXML.fxml"));
			//BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controller/FXML.fxml"));
			//Stage myStage = new Stage();
			//Parent root = FXMLLoader.load(getClass().getResource("/controller2/ControllerFXML.fxml"));
			Scene scene = new Scene(root);
			primaryStage.getIcons().add(setIcon());
			primaryStage.setTitle("Login");
			primaryStage.setScene(scene);
			primaryStage.show(); 
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	Image setIcon() {
		Image image = new Image(new File("hospital_icon.png").toURI().toString());
		return image;
	}
	public static void main(String[] args) {
		launch(args);
	}
}
