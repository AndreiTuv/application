package socket;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Animal;

public class LoginController implements Initializable{
	
	@FXML private Text userNameText;
	@FXML private Text paswordText;
	@FXML private Text errorText;
	@FXML private TextField textFieldUserName;
	@FXML private TextField textFieldPassword;
	@FXML private Button loggingButton;
	@FXML private ImageView imageView;
	@FXML private MenuItem exit;
	@FXML private PasswordField passwordField;
	@FXML private Pane pane;
	
	private Socket socket;
    private BufferedReader serverResponse;
    private PrintWriter loginDataProvider;
    private static final String loginStatus = "Succes!";
    
    @SuppressWarnings("unused")
	@Override
   	public void initialize(URL location, ResourceBundle resources) {
    try {
    	setImage();
		socket = new Socket("localhost", 5000);
		serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		loginDataProvider =  new PrintWriter(socket.getOutputStream(), true);
	} catch (IOException e) {
        System.out.println("Can't connect to server.");
	}
    	exit.setOnAction(event -> closeApp());
    	loggingButton.setOnMouseClicked(event ->{
    		String x = passwordField.getText();
    		String y = textFieldUserName.getText();
		if (passwordField.getText() != null && textFieldUserName.getText() != null) {
			String username2 = textFieldUserName.getText();
			String password2 = passwordField.getText();
			
			loginDataProvider.println(username2);
			loginDataProvider.println(password2);
			
			try {			 
				 Scene currentScene = loggingButton.getScene();
				 currentScene.getWindow().hide();
				 FXMLLoader fxmlLoader = new FXMLLoader();
				 fxmlLoader.setLocation(getClass().getResource("/controller/FXML.fxml"));
				 Scene scene = new Scene(fxmlLoader.load());
				 Stage stage = new Stage();
				 stage.setTitle("Hospital");
				 stage.setScene(scene);
				 stage.show();
			}catch (IOException e) {
		    	System.out.println("Failed to login!");
		    }
		}
	});
    }
	public void closeApp() {
		Runtime.getRuntime().addShutdownHook(new Thread(){public void run(){
		    try {
		        socket.close();
		    } catch (IOException e){ /* failed */ }
		}});
		Platform.exit();
		System.exit(0);
	}
	public void setImage() {
		try {
			pane.setStyle("-fx-border-color: blue;");
			BufferedImage image = ImageIO.read(new File("hospital-background.png"));
			Image i = SwingFXUtils.toFXImage(image,null);
			imageView.setImage(i);
			 
		} catch (IOException e) {
			System.out.println("Couldn't convert to image");
			e.printStackTrace();
		}
	}
}
